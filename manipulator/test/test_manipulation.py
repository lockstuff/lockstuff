#!/usr/bin/env python3
#

import unittest, math, sys

sys.path.append('..')

from manipulator.manipulation_data import *
from manipulator.manipulation_parser import *
from manipulator.manipulation_toolbox import *

class ToolboxTestCase(unittest.TestCase):
    """Tests to achieve full statement/branch coverage (together with session tests).
    """
    def testReadString(self):
        test_data  = ['', '', 'Blabla']
        IO.set_input(test_data)
        tv = 'Whatever...'
        self.assertEqual(read_string('Test1', tv), tv)
        self.assertEqual(read_string('Test2'), '')
        self.assertEqual(read_string('Test3'), test_data[2])

    def testReadResponse(self):
        test_data  = ['a', 'B', 'A']
        IO.set_input(test_data)
        self.assertEqual(read_response('Test1', {'a': 'a', 'A': 'A'}, 'a', lc_only=False), 'a')
        self.assertEqual(read_response('Test2', {'a': 'a', 'A': 'A'}, 'A', lc_only=False), 'A')

    def testReadNum(self):
        test_data  = ['x', 'Hello!', '5.7', 'quit']
        IO.set_input(test_data)
        self.assertTrue(math.isnan(read_num('Test1')))
        self.assertEqual(read_num('Test2'), float(test_data[2]))
        self.assertIsNone(read_num('Test3', interruptible=True))

    def testStrToNumDir(self):
        self.assertEqual(str_to_num_dir('Blablabla'), None)

    def testStrToCombination(self):
        self.assertEqual(str_to_combination('Bla-bla-bla'), None)

    def testReadCombination(self):
        test_data  = ['Bla-bla-bla', '10-20-30']
        IO.set_input(test_data)
        self.assertEqual(str(read_combination('Test1')), ' 10.0,  20.0,  30.0')

    def testReadContactPoints(self):
        test_data = ['quit', 'quit', '3.6', 'quit']
        IO.set_input(test_data)
        cp = ContactPoints(3.4, 8.6)
        self.assertIsNone(read_contact_points(interruptible=True))
        self.assertIsNone(read_contact_points(cp, interruptible=True))
        self.assertIsNone(read_contact_points(interruptible=True))

    def testGraphView(self):
        m = Manipulation()
        li = LockInfo()
        m.add_info(li)
        gv = GraphView(m, None)
        self.assertIsNone(gv.present_graphed([]))

class DataTestCase(unittest.TestCase):
    """Tests to achieve full statement/branch coverage (together with session tests).
    """
    def testContactPoints(self):
        cp = ContactPoints(3.4, 7.6)
        self.assertEqual(str(cp), '(lcp: 3.4, rcp: 7.6)')
        self.assertEqual(repr(cp), 'ContactPoints(3.4, 7.6)')

    def testCombinationNumber(self):
        cn = CombinationNumber(27.5, 'R', is_wild=True)
        self.assertEqual(repr(cn), 'CombinationNumber(27.5, "R", True)')
        self.assertEqual(str(cn), 'R27.5')
        cn = CombinationNumber(27.5)
        self.assertEqual(repr(cn), 'CombinationNumber(27.5, None, False)')

    def testCombination(self):
        cn = Combination([CombinationNumber(10, 'L'),
                          CombinationNumber(20, 'R', True),
                          CombinationNumber(30, 'L')])
        self.assertEqual(repr(cn), 'Combination([CombinationNumber(10, "L", False), '
                                   'CombinationNumber(20, "R", True), '
                                   'CombinationNumber(30, "L", False)])')
        self.assertEqual(str(cn), 'L10.0, R20.0, L30.0')
        self.assertEqual(str(cn[1]), 'R20.0')
        li = LockInfo()
        self.assertFalse(cn < cn)

    def testGraphPoint(self):
        gp = GraphPoint(56, ContactPoints(4.4, 9.3))
        self.assertEqual(repr(gp), f'GraphPoint(56, ContactPoints(4.4, 9.3))')
        self.assertEqual(str(gp), f'56 -> (lcp: 4.4, rcp: 9.3)')

    def testLockLibrary(self):
        li = LockInfo()
        li.props.pop('name')
        ll = LockLibrary()
        ll.add_info(li)
        li.add_prop('name', 'LaGard 3330')
        ll.add_info(li)
        self.assertEqual(ll.store(0), 'LOCK\n  wheels = 3\n  min = 0\n'
                                      '  max = 100\n  increment = 2.5\n'
                                      '  amplification_increment = 1\n'
                                      '  name = "LaGard 3330"')

    def testManipulation(self):
        m = Manipulation()
        g = Graph(333)
        m.add_graph(g)
        with self.assertRaises(ValueError):
            m.add_graph(g)
        

def test1(session_fname, load_fname, save_fname, locklib_fname=None, locklib_path='.'):
    IO.set_input_from_file(session_fname)
    ctrl = ManipulationController(locklib_path)
    ctrl.prepare(load_fname, locklib_fname)
    ctrl.main(save_fname)

test1('session_lib.txt', 'blabla.txt', 'saved2.txt', './lib1.txt')
test1('session_lib1.txt', 'blabla.txt', 'saved2.txt', None, './lib0.txt')
test1('session_lib2.txt', 'blabla.txt', 'saved2.txt', None, './lib1.txt')
test1('session_lib3.txt', 'blabla.txt', 'saved2.txt', None, './lib1.txt')
test1('session1.txt', 'blabla.txt', 'saved1.txt')
test1('session2.txt', 'saved1.txt', None)
test1('session3.txt', None, 'manipulation.txt', 'nonexistent_file.txt')
test1('session0.txt', 'bleble.txt', 'saved2.txt', None, './lib1.txt')

#print('\nCoverage completion:')
unittest.main()
