#!/usr/bin/env python3
#
"""Provide tools to enter and visualize graph of manipulation
of a combination lock.
"""

# import datetime
import re
import os
import math
# from abc import ABC, abstractmethod
import matplotlib.pyplot as plt

from .manipulation_data import ContactPoints, CombinationNumber, Combination,\
                              GraphPoint, HiLoTest, LockInfo, Manipulation,\
                              LockLibrary
from .manipulation_parser import parse_manipulation, parse_lock_lib

NAN = float('NaN')
EPS = 1e-5

def is_close(num1, num2):
    """To be used instead of number equality for floats.
    """
    return abs(num1 - num2) < EPS

class Io:  # pragma: no cover
    """Class allowing to automate tests of view classes,
       that require user interaction
    """
    def __init__(self):
        self.buffer = None
        self.saved = list()
        self.ptr = 0

    def clear_saved(self):
        """Clear the saved data.
        """
        self.saved = list()

    def set_input(self, data):
        """Directly set data to serve as user input.
        """
        self.buffer = data
        self.ptr = 0

    def set_input_from_file(self, fname):
        """Set data to serve as user's input.
        """
        if fname is not None:
            with open(fname, 'r') as data_file:
                # allow comments - if the first character of the line is #,
                #   ignore it
                self.buffer = [x[1:].strip()
                               for x in data_file.readlines() if x[0] != '#']
                self.ptr = 0

    def save_input(self, fname):
        """Save the user's answers.
        """
        if not self.saved:
            return
        with open(fname, 'w') as data_file:
            for line in self.saved:
                data_file.write(' '+line+'\n')
        self.clear_saved()

    def next_res(self):
        """Get the next response (if there is one), None otherwise.
        """
        res = None
        if self.ptr < len(self.buffer):
            res = self.buffer[self.ptr]
            self.ptr += 1
        return res

    def input(self, prompt):
        """If there is no answers set, ask...
           Also log the user's answers to use them in regression runs.
        """
        if self.buffer is not None:
            res = self.next_res()
            if res is not None:
                print(prompt, res)
                return res
        res = input(prompt)
        self.saved.append(res)
        return res


IO = Io()


def my_input(prompt):
    """Utility function simplifying channeling of the input via my Io class.
    """
    return IO.input(prompt)


def read_contact_points(prev=None, interruptible=False):
    """Utility function for reading contact points.
    To make entry more friendly, previous values are used as defaults,
    if provided.
    Returns list of left and right contact point values.
    """
    if prev is None:
        lcp = read_num(' Left contact point: ', interruptible=interruptible)
        if lcp is None:
            return None
        rcp = read_num(' Right contact point: ', interruptible=interruptible)
    else:
        lcp = read_num(' Left contact point: ', default=prev.lcp,
                       interruptible=interruptible)
        if lcp is None:
            return None
        rcp = read_num(' Right contact point: ', default=prev.rcp,
                       interruptible=interruptible)
    if rcp is None:
        return None
    res = ContactPoints(lcp, rcp)
    return res


def read_string(prompt, default=''):
    """Utility function to input a string from an user,
       while providing a default.
    """
    full_prompt = prompt
    if default != '':
        full_prompt += f' ({default}) '
    tmp = my_input(full_prompt)
    if tmp == '':
        return default
    return tmp


def read_response(prompt, choice_dict, default='', lc_only=True):
    """Utility function for requesting user input.
    Choices are provided in a dict, default answer can be specified
    and user input can be forced lowercase before comparison with choices.
    Returns value from the choice_dict corresponding to the user input.
    """
    while True:
        tmp_str = my_input(prompt)
        if tmp_str == '':
            tmp_str = default
        if lc_only:
            tmp_str = tmp_str.lower()
        if tmp_str not in choice_dict:
            print('Please specify one of the expected answers...')
        else:
            res = choice_dict[tmp_str]
            return res


def read_num(prompt, default=None, interruptible=False):
    """Utility function prompting user to enter a number,
    with possible default.
    """
    if default is None:
        full_prompt = prompt
    else:
        full_prompt = prompt + f'(default: {default})'
    while True:
        tmp_str = my_input(full_prompt)
        if (tmp_str == '') and (default is not None):
            res = default
            break
        if tmp_str.lower() == 'x':
            res = NAN
            break
        if interruptible and (tmp_str.lower() == 'quit'):
            res = None
            break
        try:
            res = float(tmp_str)
            break
        except ValueError:
            print('Please specify floating point number...')
    return res


NUM_DIR_REX = re.compile(r'^\s*(?P<dir>[LRlr])?\s*(?P<num>\d+(\.\d+)?)'
                         r'\s*(?P<wild>\*)?\s*$')


def str_to_num_dir(string):
    """Utility function to convert string to combination number
       (with possible direction and wildcard spec).
    """
    matched = NUM_DIR_REX.match(string)
    if not matched:
        return None
    num, direction, wild = matched.group('num', 'dir', 'wild')
    if direction is not None:
        direction = direction.upper()
    return CombinationNumber(float(num), direction, wild is not None)


def str_to_combination(string):
    """Utility function used when performing hi/lo test
    """
    res = []
    for part in re.split(r'\s*-\s*', string):
        num = str_to_num_dir(part)
        if num is None:
            return None
        res.append(num)
    result = Combination(res)
    return result


def read_combination(prompt):
    """Utility function used when performing hi/lo test
    """
    while True:
        string = my_input(prompt)
        result = str_to_combination(string)
        if result is None:
            print('Please enter a valid combination; e.g. L38.5*-R44-L26*')
            continue
        break
    return result


class HiLoTestView:
    """Class managing the user data IO.
    """
    def __init__(self, model):
        self.model = model

    def get_setup(self):
        """Get parameters of the Hi/Lo test:
             - type of the test (just high, just low or both)
             - combination with marked wildcards
             - the delta value
        """
        props = dict()
        test = read_response('What type of test to perform - '
                             'High, Lo or Both? (h/l/B)',
                             choice_dict={'h': 'h', 'l': 'l',
                                          'b': 'b'}, default='b')
        setup = read_combination('Enter a combination to test(use asterisk on'
                                 ' changing numbers - e.g. L47-R15*-L26*): ')
        delta = read_num('Enter the desired delta: ',
                         default=self.model.get_delta())
        if delta != self.model.get_delta():
            self.model.set_delta(delta)
        props['delta'] = delta
        props['test'] = test
        return (setup, props)

    @staticmethod
    def single_test(combo):
        """Performs a single test - dialing a single combo and reading
           the contactpoints.
        """
        print(f'Dial {combo}:')
        cps = read_contact_points()
        return cps

    @staticmethod
    def present_single(tests, high):
        """Present results of either Hi or Lo test.
        """
        if high:
            print('Hi test:')
        else:
            print('Lo test:')
        for test in tests:
            cps = test[1]
            delta = cps.delta()
            print(f'  {test[0]}: '
                  f'({cps.rcp:4.1f}-{cps.lcp:4.1f}) = {delta:5.2f}')

    @staticmethod
    def present_both(hi_tests, lo_tests):
        """Present results of the Hi/Lo test (both Hi and Lo parts were done).
        """
        print('Hi/Lo test:')
        for hi_test, lo_test in zip(hi_tests, lo_tests):
            hi_cps, lo_cps = hi_test[1], lo_test[1]
            hi_delta, lo_delta = hi_cps.delta(), lo_cps.delta()
            delta = (lo_delta + hi_delta)/2.0
            # print(f'  {lo_test[0]}: ({lo_cps.rcp:4.1f}-{lo_cps.lcp:4.1f}) ='
            #       f'{lo_delta:5.2f}; avg = {delta:5.2f}; {hi_delta:5.2f}'
            #       f' = ({hi_cps.rcp:4.1f}-{hi_cps.lcp:4.1f}) :{hi_test[0]}')
            print(f'  {lo_test[0]}: '
                  f'({lo_cps.rcp:4.1f}-{lo_cps.lcp:4.1f}) = {lo_delta:5.2f}\n'
                  f'  {hi_test[0]}: '
                  f'({hi_cps.rcp:4.1f}-{hi_cps.lcp:4.1f}) = {hi_delta:5.2f}\n'
                  f'    avg = {delta:5.2f}')

    def present_results(self):
        """Present the Hi/Lo test results.
        The biggest problem was to identify which test is high and which low.
        The hi/low sort is determined by comparison with the default;
        then each group is sorted itself, although high test must be sorted
        in reverse.
        """
        hi_tests, lo_tests = self.model.get_test_results()

        if hi_tests:
            if lo_tests:
                self.present_both(hi_tests, lo_tests)
            else:
                self.present_single(hi_tests, high=True)
        else:
            self.present_single(lo_tests, high=False)


class HiLoTestController:
    """Controller of the Hi/Lo test.
    """
    def __init__(self, view, model):
        self.view = view
        self.model = model

    def test_setup(self):
        """Get test setup.
        """
        setup, props = self.view.get_setup()
        self.model.set_base_combination(setup)
        for name, val in props.items():
            self.model.add_prop(name, val)

    def run_test(self):
        """Run the test.
        """
        for combo in self.model.get_test_combinations():
            cps = self.view.single_test(combo)
            self.model.add_result(combo, cps)

    def test(self):
        """Perform the whole test.
        """
        self.test_setup()
        self.run_test()
        self.view.present_results()


class LockInfoView:
    """View for the LockInfo class.
    """
    PAGE_SIZE = 3

    def __init__(self):
        self.info = LockInfo()

    @staticmethod
    def get_lib_or_manual():
        """Ask the user for the source of lock data.
        """
        decision = read_response('Do you want to load lock info from library,'
                                 ' or enter them manually? (L/m)',
                                 choice_dict={'l': 'l', 'm': 'm'},
                                 default='l')
        return decision

    def get_user_setup(self):
        """Get the lock info from the user.
        """
        info = self.info
        info.add_prop('name', read_string('Name and model of the lock:',
                                          info.get_prop('name')))
        info.add_prop('wheels', read_num('Number of wheels:',
                                         info.get_prop('wheels')))
        info.add_prop('min', read_num('Minimal number on the dial:',
                                      info.get_prop('min')))
        info.add_prop('max', read_num('Maximal number on the dial:',
                                      info.get_prop('max')))

    @staticmethod
    def get_name_hint():
        """Request part of the name to search for.
        """
        return read_string("Enter a part of the name to find\n"
                           "  (or just press enter to see the full library):")

    @staticmethod
    def pick_item(items, base, total):
        """Print out items and let user pick one.
           Or select netx/previous page, if existing
        """
        print(f'Showing {base+1}-{base+len(items)} of {total}:')
        for num, item in enumerate(items):
            print(f'  {num+1:2}: {item}')
        # possible improvement - prompt for just number if there are less
        #   than PAGE_SIZE items
        prompt = 'Enter the choice number, n/p for next/previous '\
                 'page or \'q\' to quit:'
        while True:
            res = read_string(prompt)
            res = res.lower()
            if res and (res[0] in ['n', 'p', 'q']):
                return -1, res[0]
            try:
                res = int(res) - 1
                if(res >= 0) and (res < len(items)):
                    return res, ''
            except ValueError:
                pass

    def get_choice(self, all_items):
        """Let user choose between given choices (or None)
        """
        base = 0
        total = len(all_items)
        while True:
            if base + self.PAGE_SIZE < total:
                items = all_items[base:base + self.PAGE_SIZE]
            else:
                items = all_items[base:]
            index, direction = self.pick_item(items, base, total)
            if index >= 0:
                return items[index]
            else:
                if direction == "n":
                    if base + self.PAGE_SIZE < total:
                        base = base + self.PAGE_SIZE
                elif direction == "p":
                    if base >= self.PAGE_SIZE:
                        base = base - self.PAGE_SIZE
                else:  # quit was selected
                    return None

    def present_lock(self):
        """Present the lock information and ask if user wants to update it.
        """
        info = self.info
        print(f'Lock Name: {info.get_prop("name")}')
        print(f'           {info.get_prop("wheels")} wheels')
        print(f'           dial range {info.get_prop("min")} to '
              f'{info.get_prop("max")}')
        return read_response('Do you want to update the lock info? (y/N)',
                             choice_dict={'y': 'y', 'n': 'n'},
                             default='n')

    def get_info(self, info):
        """Pass info from view to info
        """
        for prop in self.info.get_prop_names():
            info.add_prop(prop, self.info.get_prop(prop))

    def set_info(self, info):
        """Set view info from info
        """
        for prop in info.get_prop_names():
            self.info.add_prop(prop, info.get_prop(prop))


class LockInfoController:
    """Controller for the LockInfo setup.
    TODO: add a way to load lock info from a library.
    """
    def __init__(self, view, model):
        self.view = view
        self.model = model

    def user_setup(self, library):
        """Get the lock info and pass it to the user.
        """
        decision = 'm'
        lock_loaded = False
        if library.size() > 0:
            decision = self.view.get_lib_or_manual()
            if decision == 'l':
                lock_loaded = self.load_from_library(library)
            if lock_loaded:
                choice = self.view.present_lock()
                if choice == 'y':
                    decision = 'm'
            else:  # Lock was not loaded, manual setup needed
                decision = 'm'

        if decision == 'm':
            self.view.get_user_setup()
        self.view.get_info(self.model)

    def load_from_library(self, library):
        """Load lock information from a library
        """
        while True:
            hint = self.view.get_name_hint()
            matching = library.find_matching(hint)
            if matching:
                selected = self.view.get_choice(matching)
                if selected is None:
                    return False
                info = library.locks[selected]
                self.view.set_info(info)
                for prop in info.get_prop_names():
                    self.model.add_prop(prop, info.get_prop(prop))

                return True
            else:
                print(f'No item in the library matches the requested '
                      f'\'{hint}\'.')


class GraphVisualization:
    """Class facilitating display of the manipulation graph.
    """
    fig = None
    lcp_ax = None
    rcp_ax = None

    @classmethod
    def new_fig(cls):
        """Create a new figure if necessary.
        """
        if cls.fig is None:
            cls.fig, cls.lcp_ax = plt.subplots()
            cls.rcp_ax = plt.twinx()
            # self.delta_ax = plt.twinx()

    def __init__(self, limits):
        self.margin = 0.1
        self.limits = limits
        GraphVisualization.new_fig()

    def set_title(self, title):
        """Sets figure title.
        """
        self.fig.suptitle(title)

    def get_axis_limits(self, values, lcp):
        """From data compute axis limits.
        The plot of data contains two traces (LCP and RCP) and
        this function computes their respective y-axis limits in such a way,
        that the LCP is in the lower half of the plot, while RCP occupies
        the upper half of the plot.
        """
        lower = min(values)
        upper = max(values)
        delta = upper - lower
        delta = max(delta, 0.1)
        margin_below = self.margin * delta
        margin_above = (1 + 2*self.margin) * delta
        if lcp:
            return (lower - margin_below, upper + margin_above)
        return (lower - margin_above, upper + margin_below)

    def handle_graph_ends(self, sequence, numbers, lcp, rcp):
        """Provides handling of the begining/end of the graph.
           The left bound point of the graph is mirrored at the right bound
           of the graph, but the line to the right bound shouldn't be drawn
           unless the last graph point is not sufficiently close (and vice
           versa in case of graphing in an opposite direction).
        """
        # Handle wrap-around point in graph
        # Heuristics to come up with a delta, that is used the most
        delta = [x2 - x1 for x1, x2 in zip(sequence[0:-1], sequence[1:])]
        delta.sort()
        median = delta[int(len(delta) / 2) - 1]
        if is_close(numbers[0], self.limits[0]):
            if (numbers[1] - numbers[0]) > (median + EPS):
                # E.g. dialing right 0, 97.5, ...
                # We don't want to have line from 0 to 97.5...
                lcp.insert(1, NAN)
                rcp.insert(1, NAN)
                numbers.insert(1, numbers[0] + 1e-5)
            # if (self.limits[1] - numbers[-1]) < (median + EPS):
            if (self.limits[1] - numbers[-1]) < (median + EPS):
                # e.g. dialing left 0, 2.5, ...
                # We won't append point at 100, unless the graph is close
                numbers.append(self.limits[1])
                lcp.append(lcp[0])
                rcp.append(rcp[0])
        else:
            start_delta = numbers[0] - self.limits[0]
            end_delta = self.limits[1] - numbers[-1]
            outside_delta = start_delta + end_delta
            if outside_delta < (median + EPS):
                numbers.insert(0, self.limits[0] - end_delta)
                lcp.insert(0, lcp[-1])
                rcp.insert(0, rcp[-1])
                numbers.append(self.limits[1] + start_delta)
                # 1, because we just prepended!
                lcp.append(lcp[1])
                rcp.append(rcp[1])

    def plot_graph(self, points, show=True):
        """Plot the graph.
        """
        numbers = list()
        lcp = list()
        rcp = list()
        # Points must be sorted according to the dialed first
        #   this allows subsequent amplification...
        pts = dict()
        for point in points:
            pts[point.dialed] = point
        dialed_seq = sorted(pts.keys())
        for dialed in dialed_seq:
            numbers.append(dialed)
            lcp.append(pts[dialed].lcp)
            rcp.append(pts[dialed].rcp)
        tmp_seq = dialed_seq[:]
        if is_close(dialed_seq[0], self.limits[0]):
            tmp_seq.append(self.limits[1])
        if len(dialed_seq) > 1:
            self.handle_graph_ends(tmp_seq, numbers, lcp, rcp)
        # delta = [a-b for a, b in zip(rcp, lcp)]
        self.lcp_ax.clear()
        self.rcp_ax.clear()
        self.lcp_ax.plot(numbers, lcp, 'b-x')
        lim_low = max(min(numbers), self.limits[0])
        lim_high = min(max(numbers), self.limits[1])
        if len(numbers) > 1:
            self.lcp_ax.set_xlim([lim_low, lim_high])
        limits = self.get_axis_limits(lcp, True)
        # Ticks need to be pruned before setting the ylim,
        #   otherwise setting yticks messes it up.
        self.lcp_ax.set_yticks([tick for tick in self.lcp_ax.get_yticks()
                                if tick <= (sum(limits)/2)])
        self.lcp_ax.set_ylim(limits)

        self.rcp_ax.plot(numbers, rcp, 'r-x')
        if len(numbers) > 1:
            self.rcp_ax.set_xlim([lim_low, lim_high])
        limits = self.get_axis_limits(rcp, False)
        # self.delta_ax.plot(numbers, delta, 'g')
        self.rcp_ax.set_yticks([tick for tick in self.rcp_ax.get_yticks()
                                if tick >= (sum(limits)/2)])
        self.rcp_ax.set_ylim(limits)
        self.lcp_ax.grid()
        self.rcp_ax.grid()
        if show:
            plt.show(block=False)

    def save_graph(self, points, fname, w_in, h_in):
        """Save the graph as eps
        """
        #plt.close()
        self.plot_graph(points, False)
        self.fig.set_size_inches(w_in, h_in)
        plt.savefig(fname, dpi=300)


class GraphView:
    """View for the Graph class.
    """
    step = 2.5
    amp_step = 1.0

    def __init__(self, manipulation, vizualization):
        self.manipulation = manipulation
        self.viz = vizualization
        dial_min = self.manipulation.info.get_prop('min')
        dial_max = self.manipulation.info.get_prop('max')
        self.dial_limits = (dial_min, dial_max)
        self.step = GraphView.step
        self.direction = 0
        self.contacts = None
        self.wheel_action = list()

    def limit_number(self, number):
        """Adjusts the number to be within the dial range.
        """
        dial_width = self.dial_limits[1] - self.dial_limits[0]
        rem = number % dial_width
        return self.dial_limits[0] + rem

    def set_graph_number(self, number):
        """Sets graph title with graph number.
        """
        self.viz.set_title(f'Graph No. {int(number)}')

    def setup_wheel_action(self):
        """Setup the graph entry parameters.
        """
        tmp = read_response(f'Wheel action setup\n'
                            f'  l: AWL\n'
                            f'  r: AWR\n'
                            f'  w: wheel by wheel\n'
                            f'(wheel by wheel by default):',
                            {'l': 'l', 'r': 'r', 'w': 'w'}, 'w')
        if tmp == 'l':
            self.wheel_action.append('AWL')
            self.direction = 1
        elif tmp == 'r':
            self.wheel_action.append('AWR')
            self.direction = -1
        else:
            for wheel in range(int(self.manipulation.info.get_prop('wheels'))):
                action = read_string(f'Wheel {wheel+1} action:')
                self.wheel_action.append(action)

    def setup_graph_entry(self, amplify=False):
        """Let user input the manipulation parameters.
        """
        step = self.step
        if amplify:
            step = GraphView.amp_step
        start = self.dial_limits[0]
#         if self.direction == -1:
#             start, stop = stop, start
        tmp = read_response(f'Graph with default setup (from {start} '
                            f'with step of {step}) (Y/n)',
                            {'y': 'y', 'n': 'n'}, 'y')
        if tmp == 'n':
            prompt = 'Enter the first number to dial'
            if self.direction < 0:
                prompt = 'Enter the first number to dial\n' \
                         '  (upper bound of the interval to graph' \
                         ' as you will be dialing right)'
            start = read_num(prompt, start)
            self.step = read_num('Enter the step', step)
        if amplify:
            if self.step != GraphView.amp_step:
                GraphView.amp_step = self.step
        else:
            if self.step != GraphView.step:
                GraphView.step = self.step
        if self.direction == 0:
            tmp = read_response('From the first number, should I count up '
                                'or down (U/d)', {'u': 'u', 'd': 'd'}, 'u')
            self.direction = 1
            if tmp == 'd':
                self.direction = -1
        print('You are about to start entering the graph values. \n'
              '  Enter \'x\' instead of a value to indicate no measurement\n'
              '  was taken, or enter \'quit\' to interrupt the graphing.\n')
        return start

    def read_point(self, dial):
        """Read the single graph point.
        """
        print(f'Dial {dial}:')
        self.contacts = read_contact_points(self.contacts, interruptible=True)
        return self.contacts

    def show_graph(self, points):
        """Plot the manipulation graph.
        """
        self.viz.plot_graph(points)

    @staticmethod
    def follow_up():
        """Ask the user what to do next.
        """
        res = read_response(f'What do you want to do now?\n'
                            f'  c: Continue this graph\n'
                            f'  a: Amplify readings\n'
                            f'  t: Perform Hi/Lo test\n'
                            f'  f: Finish this graph\n'
                            f'  x: Exit\n'
                            f':', {'c': 'c', 'a': 'a', 't': 't',
                                   'f': 'f', 'x': 'x'})
        return res

    @staticmethod
    def get_conclusion():
        """Request the user's conclusion of the test.
        """
        return my_input('Enter the conclusion: ')

    @staticmethod
    def present_graphed(pts):
        """Determine range graphed so far.
        """
        if not pts:
            return
        lower = min(pts)
        upper = max(pts)
        print(f'Already graphed {lower}-{upper}')

    def finish(self):
        """Perform tasks when the graph is finished.
        """
        # make sure we ask for a dialing direction on the next graph
        self.direction = 0


class GraphController:
    """Controller for the Graph class.
    """
    def __init__(self, model, view):
        self.model = model
        self.view = view
        view.set_graph_number(model.number)

    def record_graph(self, start_number, amplify=False):
        """Record a single graph.
        """
        direction = self.view.direction
        step = self.view.step

        num = start_number
        for dummy in range(math.ceil(abs(self.view.dial_limits[1] -
                                         self.view.dial_limits[0]) / step)):
            if not amplify and self.model.dialed_already(num):
                break
            cps = self.view.read_point(num)
            if cps is None:
                break
            self.model.add_point(GraphPoint(num, cps))
            self.view.show_graph(self.model.points)
            num = self.view.limit_number(num + direction * step)

    def test(self):
        """Do the Hi/Lo test.
        """
        model = HiLoTest()
        view = HiLoTestView(model)
        test = HiLoTestController(view, model)
        test.test()
        self.model.add_test(model)

    def prepare(self):
        """Read in the graph setup and record the wheel action for the report.
        """
        self.view.setup_wheel_action()
        if len(self.view.wheel_action) > 1:
            for idx, action in enumerate(self.view.wheel_action):
                self.model.add_prop(f'wheel{idx+1}_action', action)
        else:
            self.model.add_prop('wheel_action', self.view.wheel_action[0])


    def main(self):
        """The main GraphController function.
        Records a graph and follows up with an amplification,
        Hi/Lo test or exit/new graph.
        """
        if not self.model.points:
            self.prepare()
            start_number = self.view.setup_graph_entry()
            self.record_graph(start_number)
        else:
            self.view.show_graph(self.model.points)
        while True:
            ans = self.view.follow_up()
            if ans == 'a':
                start_number = self.view.setup_graph_entry(amplify=True)
                self.record_graph(start_number, amplify=True)
            elif ans == 'c':
                pts = self.model.points
                self.view.present_graphed([x.dialed for x in pts])
                start_number = self.view.setup_graph_entry()
                self.record_graph(start_number)
            elif ans == 't':
                self.test()
            elif ans == 'f':
                self.view.finish()
                conclusion = self.view.get_conclusion()
                self.model.add_prop('conclusion', conclusion)
                return ans
            else:  # this covers the exit command
                return ans


# pragma pylint: disable=too-few-public-methods
class ManipulationView:
    """View of the Manipulation class.
    """
    def __init__(self):
        pass

    @staticmethod
    def what_to_do():
        """Ask user what to do now.
        """
        res = read_response(f'What do you want to do?\n'
                            f'  c: Continue with another graph\n'
                            f'  x: Exit\n'
                            f':', {'c': 'c', 'x': 'x'})
        return res


class ManipulationController:
    """Controller of the Manipulation class.
    """
    def __init__(self, path):
        self.manipulation = None
        self.view = ManipulationView()
        self.library = None
        self.library_path = path

    def load_saved(self, fname=None, locklib_fname=None):
        """Load saved manipulation.
        """
        target = 'manipulation.txt'
        if fname is not None:
            target = fname
        print(f"path = {self.library_path}")
        if os.path.isdir(self.library_path):
            locklib_target = self.library_path + '/lib.txt'
        else:
            locklib_target = self.library_path
        if locklib_fname is not None:
            locklib_target = locklib_fname
        try:
            self.library = parse_lock_lib(locklib_target)
        except FileNotFoundError:
            self.library = LockLibrary()
        try:
            self.manipulation = parse_manipulation(target)
        except FileNotFoundError:
            # for the possible future ;)
            self.manipulation = Manipulation()

    def save(self, fname=None):
        """Save the manipulation to the disk.
        """
        target = 'manipulation.txt'
        if fname is not None:
            target = fname
        with open(target, 'w') as file:
            file.write(self.manipulation.store(0))

    def prepare(self, fname=None, locklib_fname=None):
        """Get basic info on the lock being manipulated.
        """
        self.load_saved(fname, locklib_fname)
        if self.manipulation.info is None:
            model = LockInfo()
            view = LockInfoView()
            ctrl = LockInfoController(view, model)
            ctrl.user_setup(self.library)
            self.manipulation.add_info(model)

    def grapher(self):
        """The main manipulation loop - makes graphs or exits.
        """
        res = ''
        limits = (self.manipulation.info.get_prop('min'),
                  self.manipulation.info.get_prop('max'))
        viz = GraphVisualization(limits)
        model = None
        if self.manipulation.graphs:
            last = max(self.manipulation.graphs.keys())
            last_graph = self.manipulation.graphs[last]
            # check if the last graph is finished
            if last_graph.get_prop('conclusion') is None:
                model = last_graph
        while res != 'x':
            if model is None:
                model = self.manipulation.add_graph()
            view = GraphView(self.manipulation, viz)
            ctrl = GraphController(model, view)
            res = ctrl.main()
            model = None
            if res != 'x':
                res = self.view.what_to_do()

    def main(self, fname=None):
        """Main Manipulation controller function.
        """
        # self.prepare()
        self.grapher()
        plt.close('all')
        self.save(fname)
