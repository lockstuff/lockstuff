

grammar manip;

manipulation
    :  MANIPULATION lock (graph)* EOF
    ;

lockDictionary
    : (lock)* EOF
    ;

lock
    : LOCK (prop)*
    ;

graph
    : GRAPH value (graphStatement)* 
    ;

graphStatement
    : graphPoint
    | prop
    | hiLoTest
    ;
                                        
graphPoint
    : value COLON contactPoint
    ;

hiLoTest
    : HILO combination (hiLoStatement)*
    ;

hiLoStatement
    : singleTest
    | prop
    ;

singleTest
    : combination contactPoint
    ;

prop
    : NAME EQ STRING
    | NAME EQ value
    ;

contactPoint
    : LPAREN value COMA value RPAREN
    ;

combination
    : LBRACKET numberList RBRACKET
    ;

numberList
    : combinationNumber (COMA combinationNumber)*
    ;

combinationNumber
    : numDir
    | value
    ;

value
    : NUMBER
    | NAN
    ;

numDir
    : NUM_DIR
    ;

MANIPULATION
    : 'MANIPULATION'
    ;

GRAPH
    : 'GRAPH'
    ;

HILO
    : 'HI-LO'
    ;

LOCK
    : 'LOCK'
    ;

EQ
    : '='
    ;

LPAREN
    : '('
    ;

RPAREN
    : ')'
    ;

LBRACKET
    : '['
    ;

RBRACKET
    : ']'
    ;

COMA
    : ','
    ;

COLON
    : ':'
    ;

SEMICOLON
    : ';'
    ;

STRING
    : '"' (ESC | ~["\\])* '"'
    ;

fragment ESC
    : '\\' .
    ;

NUM_DIR
    : DIR NUM
    ;

fragment DIR
    : [lLrR]
    ;

NAN
    : [nN] 'a' [nN]
    ;

NAME
    : [_a-zA-Z] [_a-zA-Z0-9]*
    ;

NUMBER
    : NUM
    ;

fragment NUM
    : ('0' .. '9') + ('.' ('0' .. '9') +)?
    ;

WS
    : [ \r\n\t] + -> skip
    ;
