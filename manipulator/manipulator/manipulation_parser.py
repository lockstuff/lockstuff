#!/usr/bin/env python3
"""Module holding antlr4 parser for the manipulation data.
"""

from antlr4 import FileStream, CommonTokenStream, ParseTreeWalker
from .manipLexer import manipLexer
from .manipListener import manipListener
from .manipParser import manipParser

from . import manipulation_data


class Base:
    """Base class for the parse tree elements.
    """
    def __init__(self):
        pass

    # pragma pylint: disable=unused-argument,no-self-use
    def add_prop(self, name, value):
        """Method for adding a property.
        """
        raise ValueError  # pragma: no cover

    # pragma pylint: disable=no-self-use
    def add_value(self, value):
        """Method for adding a value.
        """
        raise ValueError  # pragma: no cover

    # pragma pylint: disable=no-self-use
    def add_graph(self, graph):
        """Method for graph addition.
        """
        raise ValueError  # pragma: no cover

    # pragma pylint: disable=no-self-use
    def add_contact_point(self, contact_point):
        """Method adding a contact point.
        """
        raise ValueError  # pragma: no cover

    # pragma pylint: disable=no-self-use
    def add_graph_point(self, graph_point):
        """Method adding a graph point.
        """
        raise ValueError  # pragma: no cover

    # pragma pylint: disable=no-self-use
    def add_combination(self, combination):
        """Method adding a combination.
        """
        raise ValueError  # pragma: no cover

    # pragma pylint: disable=no-self-use
    def add_hi_lo_test(self, test):
        """Method adding a Hi/Lo test.
        """
        raise ValueError  # pragma: no cover

    # pragma pylint: disable=no-self-use
    def add_lock_info(self, info):
        """Method adding a lock info.
        """
        raise ValueError  # pragma: no cover

    # pragma pylint: disable=no-self-use
    def create(self):
        """Method returning the constructed instance.
        """
        raise ValueError  # pragma: no cover


class PropertyPlaceholder(Base):
    """Class holding a property value.
    Since the property is not represented as a class, there is no
    create method implemented.
    """
    def __init__(self):
        super().__init__()
        self.value = None

    def add_value(self, value):
        """Record a value passed to a property.
        """
        self.value = value


class LockPlaceholder(Base):
    """Class creating a LockInfo instance bassed on the data parsed.
    """
    def __init__(self):
        super().__init__()
        self.info = manipulation_data.LockInfo()

    def add_prop(self, name, value):
        """Add a lock info proprty.
        """
        self.info.add_prop(name, value)

    def create(self):
        """Return the LockInfo instance created.
        """
        return self.info


class ManipulationPlaceholder(Base):
    """Class used to create the Manipulation instance based on the data parsed.
    """
    def __init__(self):
        super().__init__()
        self.man = manipulation_data.Manipulation()

    def add_graph(self, graph):
        """Add a new graph to the manipulation.
        """
        self.man.add_graph(graph.create())

    def add_lock_info(self, info):
        """Add info on the lock being manipulated.
        """
        self.man.add_info(info.create())

    def create(self):
        """Return the newly created Manipulation instance.
        """
        return self.man


class LockDictPlaceholder(Base):
    """Class used to create a lock library.
    """
    def __init__(self):
        super().__init__()
        self.locks = manipulation_data.LockLibrary()

    def add_lock_info(self, info):
        """Adds a info on a particular lock.
        """
        self.locks.add_info(info.create())

    def create(self):
        """Returns the newly created LockLibrary instance.
        """
        return self.locks


class GraphPlaceholder(Base):
    """Class used to create a Graph instance based on the data parsed.
       Since the graph constructor needs the graph number, the instance
       is not created rightaway, but only after the value is acquired.
    """
    def __init__(self):
        super().__init__()
        self.graph = None

    def add_prop(self, name, value):
        """Add a property to a graph.
        """
        self.graph.add_prop(name, value)

    def add_graph_point(self, graph_point):
        """Add a graph point to a graph.
        """
        self.graph.add_point(graph_point.create())

    def add_value(self, value):
        """The value is the graph number.
        """
        self.graph = manipulation_data.Graph(value)

    def add_hi_lo_test(self, test):
        """Add a Hi/Lo test.
        """
        self.graph.add_test(test.create())

    def create(self):
        """Return the freshly created Graph instance.
        """
        return self.graph


class GraphPointPlaceholder(Base):
    """Class used to create a Graph point instance based on the data parsed.
    """
    def __init__(self):
        super().__init__()
        self.value = None
        self.contact_point = None

    def add_value(self, value):
        """Adding the number dialed.
        """
        self.value = value

    def add_contact_point(self, contact_point):
        """Adding a contact points.
        """
        self.contact_point = contact_point

    def create(self):
        """Returns a freshly created GraphPoint instance.
        """
        return manipulation_data.GraphPoint(self.value,
                                            self.contact_point.create())


class ContactPointPlaceholder(Base):
    """Class used to create a ContactPoints instance based on the data parsed.
    """
    def __init__(self):
        super().__init__()
        self.pts = list()

    def add_value(self, value):
        """Adds a value of one of the contact points.
        """
        self.pts.append(value)

    def create(self):
        """Returns freshly created ContactPoints instance.
        """
        return manipulation_data.ContactPoints(self.pts[0], self.pts[1])


class CombinationPlaceholder(Base):
    """Class used to create a Combination instance based on the data passed.
    """
    def __init__(self):
        super().__init__()
        self.numbers = list()

    def add_value(self, value):
        """Add a value of one of the combination numbers
           (possibly with dialing direction).
        """
        self.numbers.append(value)

    def create(self):
        """Create and return the Combination instance.
        """
        res = list()
        for num in self.numbers:
            try:
                tmp = manipulation_data.CombinationNumber(num[0], num[1])
            except TypeError as dummy:
                tmp = manipulation_data.CombinationNumber(num)
            res.append(tmp)
        return manipulation_data.Combination(res)


class HiLoTestPlaceholder(Base):
    """Class used to create the HiLoTest instance based on the data parsed.
    """
    def __init__(self):
        super().__init__()
        self.base = None
        self.test = manipulation_data.HiLoTest()
        self.comb = None

    def add_combination(self, combination):
        """Add a combination that was tested.
        """
        if self.base is None:
            self.base = combination.create()
            self.test.set_base_combination(self.base)
        else:
            self.comb = combination.create()

    def add_contact_point(self, contact_point):
        """Add contact points corresponding to the combination.
        """
        self.test.add_result(self.comb, contact_point.create())

    def add_prop(self, name, value):
        """Add a property to the HiLoTest.
        """
        self.test.add_prop(name, value)

    def create(self):
        """Returns the newly created HiLoTest instance.
        """
        return self.test


class ManipPrintListener(manipListener):
    """Listener used when the data file is parsed.
    """
    def __init__(self):
        super().__init__()
        self.node = list()
        self.manip = None
        self.lock_dict = dict()

    def enterLockDictionary(self, ctx):
        self.node.append(LockDictPlaceholder())

    def exitLockDictionary(self, ctx):
        lock_dict = self.node[-1]
        self.lock_dict = lock_dict.create()

    def enterManipulation(self, ctx):
        self.node.append(ManipulationPlaceholder())

    def exitManipulation(self, ctx):
        manip = self.node[-1]
        self.manip = manip.create()

    def enterLock(self, ctx):
        self.node.append(LockPlaceholder())

    def exitLock(self, ctx):
        lock = self.node.pop()
        self.node[-1].add_lock_info(lock)

    def enterGraph(self, ctx):
        self.node.append(GraphPlaceholder())

    def exitGraph(self, ctx):
        graph = self.node.pop()
        self.node[-1].add_graph(graph)

    def enterGraphPoint(self, ctx):
        self.node.append(GraphPointPlaceholder())

    def exitGraphPoint(self, ctx):
        graph_point = self.node.pop()
        self.node[-1].add_graph_point(graph_point)

    def enterValue(self, ctx):
        num = ctx.NUMBER()
        if num is None:
            txt = 'NaN'
        else:
            txt = num.getText()
        self.node[-1].add_value(float(txt))

    def enterProp(self, ctx):
        self.node.append(PropertyPlaceholder())

    def exitProp(self, ctx):
        prop = self.node.pop()
        if prop.value is None:
            str1 = ctx.STRING().getText()
            self.node[-1].add_prop(ctx.NAME().getText(), str1[1:-1])
        else:
            self.node[-1].add_prop(ctx.NAME().getText(), prop.value)

    def enterContactPoint(self, ctx):
        self.node.append(ContactPointPlaceholder())

    def exitContactPoint(self, ctx):
        contact_point = self.node.pop()
        self.node[-1].add_contact_point(contact_point)

    def enterHiLoTest(self, ctx):
        self.node.append(HiLoTestPlaceholder())

    def exitHiLoTest(self, ctx):
        test = self.node.pop()
        self.node[-1].add_hi_lo_test(test)

    def enterCombination(self, ctx):
        self.node.append(CombinationPlaceholder())

    def exitCombination(self, ctx):
        comb = self.node.pop()
        self.node[-1].add_combination(comb)

    def enterNumDir(self, ctx):
        text = ctx.NUM_DIR().getText()
        self.node[-1].add_value((float(text[1:]), text[0]))


def parse_manipulation(fname):
    """Parse manipulation data.
    """
    lexer = manipLexer(FileStream(fname))
    stream = CommonTokenStream(lexer)
    parser = manipParser(stream)
    tree = parser.manipulation()
    printer = ManipPrintListener()
    walker = ParseTreeWalker()
    walker.walk(printer, tree)
    return printer.manip


def parse_lock_lib(fname):
    """Parse lock library data.
    """
    lexer = manipLexer(FileStream(fname))
    stream = CommonTokenStream(lexer)
    parser = manipParser(stream)
    tree = parser.lockDictionary()
    printer = ManipPrintListener()
    walker = ParseTreeWalker()
    walker.walk(printer, tree)
    return printer.lock_dict
