#!/usr/bin/env python3
#
"""
Vernier scale generator
=======================
Vernier scale generator utility allows you to genrate custom vernier scales
exactly the way you want.

It alows you to customize target dial diameter, number of divisions and
other parameters. To allow you to combine several different scales however you
need, subsequent invocations append to already existing file.

The scales are generated as a postscript file, ready to be printed on any
postscript capable printer. Optionaly it can be converted to a PDF format
by ps2pdf or a similar tool. To retain the exact size, make sure no scaling
takes place during the print.

Basic options:
--------------
-h              prints out the help
-o output.ps    creates output file output.ps
-A              uses A4 as the target paper format (default)
-L              uses US letter target paper format
-d DIAMETER     creates scapes for the specified dial diameter (in milimeters)
-a              append the output to the existing file
-O              overwrite existing file
-r ROWS         number of rows to draw

Paper format only needs to be specified when a new file is created or when
a file is being overwritten.

Example:
./vernier_generator.py -o scales.ps -A -d 73
  Creates a file scales.ps, containing a row of scales for dial with
  diameter 73mm, drawn on a list of A4 paper.

./vernier_generator.py -o scales.ps -a -d 63.5
  Appends another row of scales to an existing file

./vernier_generator.py -o scales.ps -O -d 80.5 -r 3
  Overwrites the file scales.ps (if the file exists already) and creates
  3 rows of scales with 80.5 mm diameter. Option -O is usefull in a script,
  when testing generation of a more complex sheet.


Scales tweaking
---------------
-n NUMBERS              Allows to specify how many numbers there are on a dial.
                          This allows to generate scales for dials with more or
                          less than 100 numbers.
-s DIVISIONS            Allows to generate scales with 1/DIVISIONS precision
                          (1/10 default)
-S {plain,samu1,samu2}  Style of scales to generate:
                        plain - default, single scales
                        samu1 - based on Legend of the Samurai sheet, style 1;
                                scales for three dial diameters are aranged in
                                a circle
                        samu2 - based on Legend of the Samurai sheet, style 2;
                                similar to the previous one, but easier
                                to adapt for non-standard dial diameter

Scales apearance tweaking
-------------------------
-t THICKNESS     Line thickness to use in milimeters
-f FONT          Legend font size in milimeters
-l BASE          Base length of the scales ticks in milimeters
-N, --no_legend  Don't print scale legend (numbers above the ticks)

Miscellaneous options
---------------------
-x X             Destination paper width in milimeters
-y Y             Destination paper height in milimeters
                   The two options above allow you to create a sheet with
                   custom paper size.
-D               Turn on debugging (useful for development purposes).

Complex sheet generation example
--------------------------------
./vernier_generator.py -o scales.ps -O -A -d 63.5
./vernier_generator.py -o scales.ps -a -d 73
./vernier_generator.py -o scales.ps -a -d 83.5
./vernier_generator.py -o scales.ps -a -S samu1
./vernier_generator.py -o scales.ps -a -S samu2
"""

import argparse
import math
import re
import os.path
from abc import ABCMeta, abstractmethod

TO_INCH = 25.4 / 72.0
TO_RAD = math.pi / 180.0


def cplx(angle):
    """Creates complex number with unit length and given angle
    """
    return complex(math.cos(angle * TO_RAD),
                   math.sin(angle * TO_RAD))


class Dial:
    """Class containing ingormations about the target dial
    """
    def __init__(self, args):
        self.diameter_val = args.diameter
        self.numbers_val = args.numbers
        self.thickness_val = args.thickness

    @property
    def diameter(self):
        """Dial diameter getter
        """
        return self.diameter_val

    @property
    def numbers(self):
        """Dial numbers getter
        """
        return self.numbers_val

    @property
    def thickness(self):
        """Dial tick thickness getter
        """
        return self.thickness_val


class Paper:
    """Class containing information on the target paper
    """
    def __init__(self, args, mark_x=None, mark_y=None):
        self.margin_val = 15
        self.width_val = mark_x
        self.height_val = mark_y
        if mark_x is None:
            if args.a4:
                self.width_val = 210.0
                self.height_val = 297.0
            elif args.letter:
                self.width_val = 216.0
                self.height_val = 280.0
            else:
                self.width_val = args.width
                self.height_val = args.height
        else:
            if args.a4 or args.letter:
                print('Superfluous paper size specification ignored.')

    @property
    def margin(self):
        """Paper margin getter
        """
        return self.margin_val

    @property
    def width(self):
        """Paper width getter
        """
        return self.width_val

    @property
    def height(self):
        """Paper height getter
        """
        return self.height_val


def draw_line(out, center, begin, end):
    """Helper to simplify drawing a line.
    """
    out.append(f'x {(center.real + begin.real) / TO_INCH} add '
               f'y {(center.imag + begin.imag) / TO_INCH} add '
               'moveto')
    out.append(f'  {end.real/TO_INCH} '
               f'{end.imag/TO_INCH} rlineto')


def draw_bbox(out, width, height):
    """Helper to simplify debugging.
    """
    out.append('x y moveto')
    out.append(f'0 {height/TO_INCH} rlineto')
    out.append(f'{width/TO_INCH} 0 rlineto')
    out.append(f'0 {-height/TO_INCH} rlineto')
    out.append(f'{-width/TO_INCH} 0 rlineto')
    out.append('stroke')


def draw_arc(out, center, radius, from_angle, to_angle):
    """Draw an arc according to a given parameters.
    """
    out.append(f'x {center.real/TO_INCH} add '
               f'y {center.imag/TO_INCH} add '
               f'{radius / TO_INCH} '
               f'{from_angle} {to_angle} arc')
    out.append('0.2 setlinewidth')
    out.append('stroke')


def draw_center(out, center):
    """Draws the center of the scales, so a custom circle
       diameter can be drawn by hand.
    """
    cm_dim = 1.5
    out.append(f'x {(center.real - cm_dim) / TO_INCH} add'
               f' y {center.imag / TO_INCH} add moveto')
    out.append(f'{2*cm_dim / TO_INCH} 0 rlineto')
    out.append(f'x {center.real / TO_INCH} add'
               f' y {(center.imag - cm_dim) / TO_INCH} add moveto')
    out.append(f'0 {2*cm_dim / TO_INCH} rlineto')
    out.append('0.2 setlinewidth')
    out.append('stroke')
    out.append(f'x {center.real / TO_INCH} add '
               f'y {center.imag / TO_INCH} add '
               f'{cm_dim / TO_INCH} '
               f'0 359.999 arc')
    out.append('stroke')


def tick_len(len_dict, num):
    """Generates the ticks pattern: every fifth long, every even smaller,
       the rest is small.
    """
    if (num % 5) == 0:
        return len_dict[5]
    if (num % 2) == 0:
        return len_dict[2]
    return len_dict[1]


def tick_lengths(divisions, base_len, len_dict=None):
    """Generates the tick pattern - varied for normal vernier scales,
       uniform for Samu style 2.
    """
    if len_dict is None:
        return [base_len for x in range(divisions+1)]
    return [tick_len(len_dict, x) for x in range(divisions+1)]


class ScaleTicks:
    """Class governing the ticks and their legend.
    """
    def __init__(self, args, vern, paper):
        self.no_legend = args.no_legend
        self.font = args.font
        self.base_len = args.base
        divisions = args.divisions
        if self.font == -1:
            self.font = vern.get_font_size()
        self.gap_val = 1
        if self.no_legend:
            self.gap_val = 0
        self.height = self.font + self.gap
        self.paper = paper
        self.update_ticks(divisions, self.base_len)

    def update_ticks(self, divisions, base_len, uniform=False):
        """Updates the ticks based on given parameters.
        """
        if uniform:
            len_dict = None
        else:
            len_dict = {5: 3 * base_len,
                        2: 2 * base_len,
                        1: 1 * base_len}
        self.ticks = tick_lengths(divisions, base_len, len_dict)

    @property
    def max_ticks(self):
        """Return the largest tick length
        """
        return max(self.ticks)

    @property
    def gap(self):
        """Return size of the gap berween a tick and its legend.
        """
        return self.gap_val

    def print(self):
        """Returns whether the legend should be printed or not.
        """
        return not self.no_legend

    def set_no_legend(self):
        """Samu style scales do not have a legend, so this sets no legend.
        """
        self.no_legend = True
        self.font = 0
        self.gap_val = 0
        self.height = 0


class Vernier(metaclass=ABCMeta):
    """Class that constructs the vernier scale according to the given
       parameters.
    """

    # to make debugging the bboxes/centers of images easier
    debug = False
    desc_height = 5
    bbox_margin = 1

    def __init__(self, args, dial, paper):
        self.divisions = args.divisions
        self.dial = dial
        self.rows = args.rows
        self.radius = self.dial.diameter / 2.0
        self.step = (360.0 / self.dial.numbers) * (self.divisions - 1) \
            / self.divisions
        self.ticks = ScaleTicks(args, self, paper)
        self.r_max = self.ticks.max_ticks
        if self.ticks.print():
            self.r_max += self.ticks.height

    def get_font_size(self):
        """Compute optimal font size for the tick legend.
        """
        max_desc_width = (len(f'{self.divisions}') +
                          len(f'{self.divisions-1}')) / 2.0
        arc_len = self.radius * (self.step / 180.0 * 2.0 * math.pi)
        # add some margin for the space between the labels
        font_width = arc_len / (1.6 * max_desc_width)
        font_height = 1.5 * font_width
        #  print(f'max_desc = {max_desc_width}, arc_len = {arc_len}, '
        #        f'font = {font_height}')
        return font_height

    @classmethod
    def find_current_y(cls, file):
        """Method that searches existing postscript file for the
           comment indicating the current Y position where the
           scales can be drawn and also rewinds the current file
           position just before the last showpage command.
        """
        width = None
        height = None
        curr_y = None
        last_showpage = None
        pos_rex = re.compile(r'^% vernier: '
                             r'paper_x = (?P<x>\d+(\.\d+)?) '
                             r'paper_y = (?P<y>\d+(\.\d+)?) '
                             r'current_y = (?P<curr_y>\d+(\.\d+)?)')
        show_page = re.compile(r'^showpage$')
        while True:
            current_pos = file.tell()
            line = file.readline()
            if len(line) == 0:
                break
            matched = pos_rex.match(line)
            if matched:
                width = float(matched.group('x'))
                height = float(matched.group('y'))
                curr_y = float(matched.group('curr_y'))
            else:
                matched = show_page.match(line)
                if matched:
                    last_showpage = current_pos
        if last_showpage is not None:
            file.seek(last_showpage)
        return (width, height, curr_y)

    def create(self, file, y_base=-1):
        """Creates the postscript file and draws the scales repeatedly
           according to the request. Appends a mark and issues a command
           to draw the page.
        """
        proc, bbox_x, bbox_y = self.vernier_proc()
        rep_x = math.floor((self.ticks.paper.width -
                            2*self.ticks.paper.margin) / bbox_x)
        if y_base == -1:
            file.write("%!PS\n")
            file.write("/cshow { dup stringwidth pop -0.5 mul 0 rmoveto"
                       " show } def\n")

            y_base = self.ticks.paper.height - self.ticks.paper.margin
        if y_base - (self.rows * bbox_y + self.desc_height) < \
           self.ticks.paper.margin:
            # print("\nUnfortunately the page doesn't have enough empty space "
            #       "for this.")
            # return
            file.write('showpage\n')
            y_base = self.ticks.paper.height - self.ticks.paper.margin
        file.write('\n'.join(proc) + "\n")
        pos_y = y_base
        for dummy in range(self.rows):
            pos_y -= bbox_y
            for cnt_x in range(rep_x):
                pos_x = self.ticks.paper.margin + bbox_x * cnt_x
                file.write(f'{pos_x/TO_INCH} {pos_y/TO_INCH} vernier\n')
                if self.debug:
                    file.write(f'{pos_x/TO_INCH} {pos_y/TO_INCH} moveto\n')
                    file.write('-10 -10 rmoveto\n')
                    file.write('20 20 rlineto\n')
                    file.write('-20 0 rmoveto\n')
                    file.write('20 -20 rlineto\n')
                    file.write('stroke\n')
        file.write(f'/Helvetica {2/TO_INCH} selectfont\n')
        file.write(f'{self.ticks.paper.margin/TO_INCH} '
                   f'{(pos_y - (self.desc_height - 2))/TO_INCH} '
                   'moveto\n')
        file.write('(')
        file.write(self.long_desc() + ') show\n')
        file.write(f'% vernier: paper_x = {self.ticks.paper.width} '
                   f'paper_y = {self.ticks.paper.height} '
                   f'current_y = {pos_y-self.desc_height}\n')
        file.write('showpage\n')

    @classmethod
    @abstractmethod
    def short_desc(cls):
        """Placeholder for the short description used for help message.
        """
        ...
        # return ""  # pragma: no cover

    @abstractmethod
    def long_desc(self):
        """Placeholder for the long description that is printed below
           the vernier scales.
        """
        ...
        # return ""  # pragma: no cover

    @abstractmethod
    def get_bbox(self, radius, r_max, angle):
        """BBox computation placeholder
        """
        ...
        # return (0, 0)  # pragma: no cover

    def draw_vernier(self, out, center, radius=-1, start_angle=90):
        """Draws a single vernier scale.
        """
        if radius == -1:
            radius = self.radius
        max_tick = self.ticks.max_ticks
        for i in range(self.divisions+1):
            # going clockwise, hence the minus sign
            angle = start_angle - self.step * i
            direction = cplx(angle)
            start_pt = center + radius * direction
            out.append(f'x {start_pt.real/TO_INCH} add '
                       f'y {start_pt.imag/TO_INCH} add moveto')
            tick = direction * self.ticks.ticks[i]
            out.append(f'  {tick.real/TO_INCH} {tick.imag/TO_INCH} rlineto')
            if self.ticks.print():
                num_base = direction * (self.ticks.gap + max_tick -
                                        self.ticks.ticks[i])
                out.append(f'  {num_base.real/TO_INCH}'
                           f' {num_base.imag/TO_INCH}'
                           ' rmoveto')
                out.append("gsave")
                out.append(f"{angle - 90} rotate")
                out.append(f"({i}) cshow")
                out.append("grestore")
        out.append(f'{self.dial.thickness} setlinewidth')
        out.append('stroke')
        angle1 = start_angle - self.step * self.divisions
        draw_arc(out, center, radius, angle1, start_angle)

        return angle

    @abstractmethod
    def vernier(self, out):
        """Placeholder for the meat of the vernier drawing procedure.
        """
        ...
        # return (out, 0, 0)  # pragma: no cover

    def vernier_proc(self):
        """Assembles the vernier drawing procedure.
        """
        out = []
        out.append('/vernier {')
        # variables are on the stack, so they are reversed
        out.append('  /y exch def')
        out.append('  /x exch def')
        res = self.vernier(out)
        out.append('} def')
        return res


class Plain(Vernier):
    """Class that produces a plain vernier scale.
    """

    @classmethod
    def short_desc(cls):
        """The short description used for help message.
        """
        return "single scale"

    def long_desc(self):
        """The long description that is printed below
           the vernier scales.
        """
        return(f'Dial diameter {self.dial.diameter:.1f}mm, {self.dial.numbers}'
               f' numbers, {self.divisions} divisions,'
               f' tick {self.dial.thickness:.1f}mm thick,'
               f' font height {self.ticks.font:.1f}mm,'
               f' tick length base {self.ticks.base_len:.1f}mm')

    def get_bbox(self, radius, r_max, angle):
        """
        Get bounding box of the single vernier scale.
        It should be general, so the scale itself can extend over 90 degrees.
        """
        min_x = -self.ticks.font
        if angle < 90.0:
            max_x = (radius + r_max) * math.sin(angle * TO_RAD)
        else:
            max_x = radius + r_max
            if angle > 270.0:
                min_x = -(radius + r_max)
            elif angle > 180.0:
                min_x = (radius + r_max) * math.sin(angle * TO_RAD)
        max_y = radius + r_max + self.ticks.font
        if angle > 180.0:
            min_y = -(radius + r_max)
        else:
            if angle < 90.0:
                min_y = radius * math.cos(angle * TO_RAD)
            else:
                min_y = (radius + r_max) * math.cos(angle * TO_RAD)
        min_x -= self.bbox_margin
        max_x += self.bbox_margin
        min_y -= self.bbox_margin
        max_x += self.bbox_margin
        return (min_x, min_y, max_x, max_y)

    def vernier(self, out):
        """Puts together the actual implementation of the vernier procedure.
        """
        out.append(f'/Helvetica {self.ticks.font/TO_INCH} selectfont')
        out.append(f'{self.dial.thickness} setlinewidth')
        out.append('newpath')
        # compute the approximate bbox
        (bbox_x1, bbox_y1, bbox_x2, bbox_y2) = \
            self.get_bbox(radius=self.radius, r_max=self.r_max,
                          angle=self.step * self.divisions)
        width = bbox_x2 - bbox_x1
        height = bbox_y2 - bbox_y1

        center = complex(-bbox_x1, (height - self.r_max)-self.radius - 1)
        self.draw_vernier(out, center)
        if self.debug:
            draw_bbox(out, width, height)
        return (out, width, height)


class Samu(Vernier):
    """Base class for creating Vernier scales based on sheet made
       by the LegendOfTheSamurai.
    """
    def __init__(self, args, dial, paper):
        super().__init__(args, dial, paper)
        self.diameters = [
                           ((2.5,), "small dials (Mosler, Yale, Big Red)"),
                           ((2.9, 2.95), "Mosler and most S&G"),
                           ((3.17,), "Big Red and LaGard")
                         ]
        diameter_list = []
        for diameter, dummy in self.diameters:
            diameter_list.extend(diameter)
        self.samu_d = [x * 25.4 for x in diameter_list]
        self.ticks.set_no_legend()
        self.r_max = self.ticks.max_ticks
        self.multiple_ticks = True

    @classmethod
    @abstractmethod
    def short_desc(cls):
        """Placeholder for the short description used for help message.
        """
        ...
        # return ""  # pragma: no cover

    @abstractmethod
    def long_desc(self):
        """Placeholder for the long description that is printed below
           the vernier scales.
        """
        ...
        # return ""  # pragma: no cover

    def diameters_description(self):
        """Generates description of the diameters used by the scales.
        """
        desc = []
        for diameter_tuple, label in self.diameters:
            diameter_list = [f'{x:.1f}"' for x in diameter_tuple]
            desc.append('/'.join(diameter_list) + f' - {label}')
        full = ', '.join(desc)
        return full

    def draw_dial(self, out, center):
        """Draws the inner part, where the lines are at positions
           where the numbers would be on a dial. Helps checking
           the implementation is actually correct.
        """
        # draw the inner part with dial.numbers graduations
        out.append('0.2 setlinewidth')
        for i in range(int(self.dial.numbers)):
            angle = 90 - i * (360.0 / self.dial.numbers)
            direction = cplx(angle)
            draw_line(out, center, direction * 0.4 * self.samu_d[0],
                      direction * self.samu_d[0] / 10)
        out.append('stroke')
        out.append(f'x {center.real / TO_INCH} add '
                   f'y {center.imag / TO_INCH} add '
                   f'{0.4 * self.samu_d[0] / TO_INCH} '
                   f'0 359.999 arc')
        out.append('stroke')

    def get_bbox(self, radius, r_max, angle):
        """Computes bbox of the scales.
        """
        # print(radius, r_max)
        max_x = radius + r_max + self.bbox_margin
        max_y = max_x
        min_x = -max_x
        min_y = min_x
        return (min_x, min_y, max_x, max_y)

    def vernier(self, out):
        """Puts together the actual implementation of the vernier procedure.
        """
        out.append(f'{self.dial.thickness} setlinewidth')
        out.append('newpath')
        # compute the approximate bbox
        (bbox_x1, bbox_y1, bbox_x2, bbox_y2) = \
            self.get_bbox(radius=self.radius, r_max=self.r_max,
                          angle=self.step * self.divisions)
        width = bbox_x2 - bbox_x1
        height = bbox_y2 - bbox_y1
        # print(bbox_y1, bbox_y2, r, r_max, height)

        center = complex(width/2, height/2)
        draw_center(out, center)
        self.draw_dial(out, center)
        start_number = 0
        end = self.dial.numbers - self.divisions + 1
        while start_number < end:
            start_angle = 90.0 - start_number * 360.0 / self.dial.numbers
            angle1 = start_angle - self.step * self.divisions
            if self.multiple_ticks:
                for diameter in self.samu_d:
                    self.draw_vernier(out, center, diameter/2.0, start_angle)
            else:
                self.draw_vernier(out, center, self.samu_d[0]/2.0,
                                  start_angle)
                for diameter in self.samu_d:
                    draw_arc(out, center, diameter/2, angle1, start_angle)
            start_number += self.divisions
        if self.debug:
            draw_bbox(out, width, height)
        return (out, width, height)


class Samu1(Samu):
    """Class creating the first style of the Samu's Vernier scales.
       This one has separate scales for each diameter.
    """
    def __init__(self, args, dial, paper):
        super().__init__(args, dial, paper)
        self.high_bound = self.samu_d[-1]
        self.radius = self.high_bound / 2.0

    @classmethod
    def short_desc(cls):
        """The short description used for help message.
        """
        return "Legend of the Samurai style 1"

    def long_desc(self):
        """The long description that is printed below
           the vernier scales.
        """
        full = self.diameters_description()
        return f'Legend of the Samurai style 1, {full}'


class Samu2(Samu):
    """Class creating the secons style of the Samu's Vernier scales.
       This one is easier to modify for a different dial diameter,
       as the ticks are in one single line.
    """
    def __init__(self, args, dial, paper):
        super().__init__(args, dial, paper)
        high_bound = max(self.dial.diameter, self.samu_d[-1])
        base_len = self.ticks.base_len + (high_bound - self.samu_d[0]) / 2.0
        self.ticks.update_ticks(self.divisions, base_len, uniform=True)
        self.radius = self.samu_d[0] / 2.0
        self.r_max = self.ticks.max_ticks
        self.multiple_ticks = False

    @classmethod
    def short_desc(cls):
        """The short description used for help message.
        """
        return "Legend of the Samurai style 2"

    def long_desc(self):
        """Placeholder for the long description that is printed below
           the vernier scales.
        """
        full = self.diameters_description()
        if self.dial.diameter > self.samu_d[-1]:
            full += f', extending to {self.dial.diameter/25.4:.2f}"'
        return f'Legend of the Samurai style 2, {full}'


STYLES = {'plain': Plain,
          'samu1': Samu1,
          'samu2': Samu2}


def work(args):
    """Performs basic arguments sanity checking and creates the scales
       according to them.
    """
    if args.output == "":
        print('Please specify the output file.')
        return
    dial = Dial(args)
    if not args.append:
        if os.path.isfile(args.output) and not args.overwrite:
            print('\nCowardly refusing to overwrite existing file without'
                  ' an explicit permission (--overwrite).')
            return
        with open(args.output, 'w', encoding='UTF8') as file:
            paper = Paper(args)
            inst = STYLES[args.style](args, dial, paper)
            inst.create(file)
    else:
        with open(args.output, 'r+', encoding='UTF8') as file:
            paper_x, paper_y, curr_y = Vernier.find_current_y(file)
            if curr_y is None:
                print('I was not able to find the current_y mark, '
                      "so I can't continue.")
                return
            paper = Paper(args, paper_x, paper_y)
            inst = STYLES[args.style](args, dial, paper)
            inst.create(file, y_base=curr_y)


def main():
    """Function creating the argument parser, parsing the arguments and
       finally calls a worker to do what is needed.
    """
    arg_parser = argparse.ArgumentParser(
        description='Vernier scale generator '
                    'generates scales that aid precise measurements needed for'
                    'combination lock manipulation. Subsequent runs append to'
                    'an existing file (unless instructed to overwrite),'
                    'allowing you to combine as many different scales as'
                    'you want.')
    arg_parser.add_argument('-n', '--numbers', dest='numbers', type=float,
                            default=100, help='number of steps of the dial')
    arg_parser.add_argument('-d', '--diameter', dest='diameter', type=float,
                            default=75, help='dial diameter [milimeters]')
    arg_parser.add_argument('-s', '--divisions', dest='divisions', type=int,
                            default=10, help='number of divisions per step')
    arg_parser.add_argument('-t', '--thickness', dest='thickness', type=float,
                            default=1, help='line thickness [milimeters]')
    arg_parser.add_argument('-f', '--font-height', dest='font', type=float,
                            default=-1, help='font height [milimeters]')
    arg_parser.add_argument('-l', '--length', dest='base', type=float,
                            default=1, help='base tick length [milimeters]')
    arg_parser.add_argument('-r', '--rows', dest='rows', type=int, default=1,
                            help='number of rows to print')
    arg_parser.add_argument('-N', '--no_legend', dest='no_legend',
                            action="store_true", default=False,
                            help="Don't print legend (numbers above ticks)")
    arg_parser.add_argument('-o', '--output', dest='output', type=str,
                            default="", help='name of the output file')
    arg_parser.add_argument('-O', '--overwrite', dest='overwrite',
                            default=False, action='store_true',
                            help='overwrite existing file')
    arg_parser.add_argument('-a', '--append', dest='append',
                            action="store_true", default=False,
                            help='append the output to the existing file')
    arg_parser.add_argument('-A', '--a4', dest='a4', default=False,
                            action='store_true',
                            help='destination is a4 paper')
    arg_parser.add_argument('-L', '--letter', dest='letter', default=False,
                            action='store_true',
                            help='destination is US letter paper')
    arg_parser.add_argument('-x', '--paper_width', dest='x', default=210.0,
                            type=float,
                            help='destination paper width in milimeters')
    arg_parser.add_argument('-y', '--paper_height', dest='y', default=297.0,
                            type=float,
                            help='destination paper height in milimeters')
    styles = ', '.join([f'{x} ({y.short_desc()})'
                        for x, y in STYLES.items()])
    arg_parser.add_argument('-S', '--style', dest='style', default='plain',
                            choices=STYLES.keys(),
                            help=f'vernier style to generate: {styles}')
    arg_parser.add_argument('-D', '--debug', dest='debug',
                            action="store_true", default=False,
                            help="Turn on debugging.")

    args = arg_parser.parse_args()
    Vernier.debug = args.debug
    work(args)


if __name__ == '__main__':
    main()

#
