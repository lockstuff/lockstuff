"""Manipulation data module holds all the data needed when
manipulation is recorded.
"""


class PropertyHolder:
    """Class holding the properties.
    """
    def __init__(self, props=None):
        if props is not None:
            self.props = dict(props)
        else:
            self.props = dict()

    def add_prop(self, name, value):
        """Add a property and its value.
        """
        self.props[name] = value

    def get_prop(self, name):
        """Retieve a property value (None if not existing).
        """
        if name in self.props:
            return self.props[name]
        return None

    def get_prop_names(self):
        """Get all property names the object holds.
        """
        return self.props.keys()

    def dump_props(self):
        """Return a list of strings with a representation of the class that can be
           parsed back.
        """
        res = list()
        for prop_name, prop_val in self.props.items():
            if isinstance(prop_val, str):
                res.append(f"  {prop_name} = \"{prop_val}\"")
            else:
                res.append(f"  {prop_name} = {prop_val}")
        return res


class ContactPoints:
    """Class containing contact points information
    """
    def __init__(self, lcp, rcp):
        self._rcp = rcp
        self._lcp = lcp

    def __repr__(self):
        return f'ContactPoints({self.lcp}, {self.rcp})'

    def __str__(self):
        return f'(lcp: {self.lcp}, rcp: {self.rcp})'

    @property
    def rcp(self):
        """Return right contact point.
        """
        return self._rcp

    @property
    def lcp(self):
        """Return left contact point.
        """
        return self._lcp

    def delta(self):
        """Compute the gap between left and right contact points
           (allowing for wrap around).
        """
        lock = LockInfo.get_current_lock()
        dummy, max_num = lock.get_min_max()
        res = abs(self.rcp - self.lcp)
        if res > lock.get_mid():
            res = max_num - res
        return res


def limit_value(val, lo_limit=0, hi_limit=100):
    """Limit the combination number value
    """
    res = lo_limit + val % (hi_limit - lo_limit)
    return res


class CombinationNumber:
    """Class representing a combination number
    """
    def __init__(self, number, direction=None, is_wild=False):
        self.number = number
        self.direction = None
        if direction is not None:
            self.direction = direction.upper()
        self.is_wild = is_wild

    def __repr__(self):
        direction = None
        if self.direction is not None:
            direction = f'"{self.direction}"'
        return f'CombinationNumber({self.number}, {direction}, {self.is_wild})'

    def __str__(self):
        direction = ''
        if self.direction is not None:
            direction = self.direction
        res = f"{direction}{self.number:.1f}"
        res_len = len(res)
        if res_len < 5:
            res = ' '*(5-res_len) + res
        return res

    def is_wild_card(self):
        """Returns a bool based on the setting of a number as a wildcard
           (number to be subjected to the Hi/Lo test).
        """
        return self.is_wild

    def increment(self, delta):
        """Adds a delta to the number (with limitation).
        """
        self.number = limit_value(self.number + delta)

    def get_delta(self, other):
        """Find the shortest distance from this to the other number.
        """
        num1, num2 = self.number, other.number
        delta = num2 - num1
        lock = LockInfo.get_current_lock()
        dummy, max_num = lock.get_min_max()
        mid = lock.get_mid()
        if delta > mid:
            delta = delta - max_num
        if delta < (-1 * mid):
            delta = delta + max_num
        return delta

    def __lt__(self, other):
        delta = self.get_delta(other)
        res = False
        if (abs(delta) > 1e-6) and (delta > 0):
            res = True
        # print(f'{self.number} < {other.number} is {res} (delta = {delta})')
        return res

    def __eq__(self, other):
        delta = self.get_delta(other)
        if abs(delta) < 1e-6:
            return True
        return False


class Combination:
    """Class representing a combination
    """
    def __init__(self, numbers):
        self.numbers = numbers

    def __repr__(self):
        return f'Combination({self.numbers})'

    def __str__(self):
        return ', '.join([str(x) for x in self.numbers])

    def __getitem__(self, key):
        return self.numbers[key]

    def subst(self, i, delta):
        """Returns a combination, in which a number at index i
            is changed by delta
        """
        tmp = self.numbers[:]
        # deep copy the number to change
        old = tmp[i]
        tmp[i] = CombinationNumber(old.number, old.direction, old.is_wild)
        tmp[i].increment(delta)
        return Combination(tmp)

    def len(self):
        """Return number of values in a combination
        """
        return len(self.numbers)

    def is_number_wild(self, num):
        """Return true if a number at position num is a wildcard.
        """
        return self.numbers[num].is_wild_card()

    def __lt__(self, combo):
        res = False
        for number1, number2 in zip(self.numbers, combo.numbers):
            if number1 != number2:
                return bool(number1 < number2)
        return res


class HiLoTest(PropertyHolder):
    """Class holding the Hi-Lo-test data
    """
    delta = 10

    @classmethod
    def get_delta(cls):
        """Return a value to use as a delta in a test.
        """
        return cls.delta

    @classmethod
    def set_delta(cls, delta):
        """Set a value to use as a delta in a test.
        """
        cls.delta = delta

    def __init__(self):
        super().__init__()
        self.base = None
        self.results = list()

    def add_result(self, combination, contact_points):
        """Add a hi/lo single test result.
        """
        self.results.append((combination, contact_points))

    def get_base_combination(self):
        """Return the base combination for the Hi/Lo test.
        """
        return self.base

    def set_base_combination(self, combination):
        """Set the test base combination.
        """
        self.base = combination

    def get_test_combinations(self):
        """Return a list of all combinations to test.
        """
        test = self.get_prop('test')
        delta = self.get_prop('delta')
        res = list()
        for wheel in range(self.base.len()):
            if not self.base.is_number_wild(wheel):
                continue
            hi_test, lo_test = None, None
            if test != 'l':
                hi_test = self.base.subst(wheel, delta)
            if test != 'h':
                lo_test = self.base.subst(wheel, -1*delta)
            # sequence the combinations so the dialing is minimized
            if self.base.numbers[wheel].direction == 'R':
                res += [hi_test, lo_test]
            else:
                res += [lo_test, hi_test]
        # just get rid of those None elements
        return [combo for combo in res if combo is not None]

    def get_test_results(self):
        """Sorts test results into hi and lo tests lists.
        """
        default_combo = self.get_base_combination()

        hi_tests, lo_tests = list(), list()
        for res in self.results:
            if res[0] > default_combo:
                hi_tests.append(res)
            else:
                lo_tests.append(res)
        # print(f'Hi tests: {hi_tests}\n\n')
        # print(f'Lo tests: {lo_tests}\n\n')
        hi_tests.sort(key=lambda a: a[0], reverse=True)
        lo_tests.sort(key=lambda a: a[0])
        return hi_tests, lo_tests

    def store(self, level):
        """Return a string with a representation of the class that can be
           parsed back.
        """
        res = list()
        space = "  "*level
        res.append(space + f"HI-LO [{self.base}]")
        res += self.dump_props()
        for test in self.results:
            res.append(f"  [{str(test[0])}] {test[1].lcp, test[1].rcp}")
        return ("\n"+space).join(res)


class GraphPoint:
    """Class holding a single point of a manipulation graph.
    Holds number dialed, contact points and a flag signifying
    the point was added in the current session.
    """
    def __init__(self, dialed, contact_points):
        self.dialed = dialed
        self.contact_points = contact_points

    def __repr__(self):
        return f'GraphPoint({self.dialed}, {repr(self.contact_points)})'

    def __str__(self):
        return f'{self.dialed} -> {str(self.contact_points)}'

    def store(self, level):
        """Return a string with a representation of the class that can be
           parsed back.
        """
        return ("  "*level) + f"{self.dialed}: ({self.lcp}, {self.rcp})"

    @property
    def lcp(self):
        """Return left contact point.
        """
        return self.contact_points.lcp

    @property
    def rcp(self):
        """Return right contact point.
        """
        return self.contact_points.rcp


class Graph(PropertyHolder):
    """Class representing a set of number and associated left and right
    contact point values.
    To allow incremental work, added points are differentiated to allow
    only the new values to be saved.
    """

    def __init__(self, number):
        """Graphs are numbered, to allow easy identification of the last graph.
        """
        super().__init__()
        self.number = number
        # we need it to store the sequence as it was entered
        self.points = list()
        self.dialed_set = set()
        self.hi_lo_test = list()

    def store(self, level):
        """Return a string with a representation of the class that can be
           parsed back.
        """
        res = list()
        space = "  "*level
        res.append(space + f"GRAPH {int(self.number)}")
        res += self.dump_props()
        for point in self.points:
            res.append(point.store(1))
        for test in self.hi_lo_test:
            res.append(test.store(2))
        return ("\n"+space).join(res)

    def add_point(self, point):
        """Adds a point to a graph.
        """
        self.points.append(point)
        self.dialed_set.add(int(100 * point.dialed))

    def dialed_already(self, number):
        """Check if the number was dialed already.
        """
        res = int(100 * number) in self.dialed_set
        if not res:
            lock = LockInfo.get_current_lock()
            min_num, max_num = lock.get_min_max()
            if min(abs(number - min_num), abs(number - max_num)) < 1e-6:
                res = res or int(100 * min_num) in self.dialed_set
                res = res or int(100 * max_num) in self.dialed_set
        return res

    def add_test(self, test):
        """Add a Hi/Lo test to thr graph.
        """
        self.hi_lo_test.append(test)


class LockInfo(PropertyHolder):
    """Class holding the info on the lock
    """
    current_lock = None

    def __init__(self):
        super().__init__(
            props={
                'name': 'S\\&G 6741',
                'wheels': 3,
                'min': 0,
                'max': 100,
                'increment': 2.5,
                'amplification_increment': 1})
        LockInfo.current_lock = self

    @classmethod
    def get_current_lock(cls):
        """To allow easy access to the info on lock.
        """
        return cls.current_lock

    def get_min_max(self):
        """Helper function to easily extract minimum and maximum dial values.
        """
        return (self.get_prop('min'), self.get_prop('max'))

    def get_mid(self):
        """Helper function to easily get middle of the dial value.
        """
        limits = self.get_min_max()
        return sum(limits)/2

    def store(self, level):
        """Return a string with a representation of the class that can be
           parsed back.
        """
        res = list()
        space = "  "*level
        res.append(space + f"LOCK")
        res += self.dump_props()
        return ("\n"+space).join(res)


class LockLibrary:
    """Class holding library of lock information.
    """
    def __init__(self):
        self.locks = dict()

    def size(self):
        """Return number of locks stored.
        """
        return len(self.locks)

    def add_info(self, info):
        """Adds a new lock info to the library.
        """
        name = info.get_prop('name')
        if name is not None:
            self.locks[name] = info

    def find_matching(self, hint):
        """Find all names matching the hint.
        """
        hint = hint.lower()
        return [name for name in self.locks if name.lower().find(hint) != -1]

    def store(self, level):
        """Return a string with a representation of the class that can be
           parsed back.
        """
        res = list()
        for dummy, info in self.locks.items():
            res.append(info.store(0))
        return ("\n"+" "*level).join(res)


class Manipulation:
    """Main class - loads, holds and saves the data
    """
    def __init__(self):
        self.info = None
        self.graphs = dict()

    def add_info(self, info):
        """Add info on lock being manipulated
        """
        self.info = info

    def add_graph(self, graph=None):
        """Create a new graph making sure its ID doesn't clash with
        existing ones.
        """
        if graph is None:
            if self.graphs:
                num = max(self.graphs.keys()) + 1
            else:
                num = 1
            graph = Graph(num)
        else:
            if graph.number in self.graphs:
                print('Trying to add a graph that already exists.')
                raise ValueError
            num = graph.number
        self.graphs[num] = graph
        return graph

    def store(self, level):
        """Return a string with a representation of the class that can be
           parsed back.
        """
        res = list()
        res.append("MANIPULATION")
        res.append(self.info.store(1))
        for dummy, graph in self.graphs.items():
            res.append(graph.store(level + 1))
        return ("\n"+" "*level).join(res)
