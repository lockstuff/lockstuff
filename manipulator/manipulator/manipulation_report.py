#!/usr/bin/env python3
#
"""Module used to generate a latex report from the saved manipulation data.
"""
import argparse
from . import manipulation_parser
from . import manipulation_toolbox


class BackEnd:
    """
    Class for prototypes of a report generation backends.
    """
    #def __init__(self):
    #    pass

    def hi_lo_report_single(self, label, tests):
        """Present results of either Hi or Lo test.
        """
        pass  # pragma: no cover

    def hi_lo_report_both(self, label, hi_tests, lo_tests):
        """Present results of the Hi/Lo test (both Hi and Lo parts were done).
        """
        pass  # pragma: no cover

    def graph_report_action(self, graph):
        """Generate the latex code for the wheel action report.
        """
        pass  # pragma: no cover

    def graph_save_pic(self, graph, viz):
        """Save the graph to an eps file and return its name.
        """
        pass  # pragma: no cover

    def graph_report(self, graph, viz):
        """Generate the graph report.
        """
        pass  # pragma: no cover

    def manipulation_report(self, info, graph_reports):
        """Generate a code for the manipulation report.
        """
        pass  # pragma: no cover

class LatexBackend(BackEnd):
    """
    Latex backend report generator class.
    """
    def __init__(self):
        #super().__init__()
        pass

    def hi_lo_report_single(self, label, tests):
        """Present results of either Hi or Lo test.
        """
        res = [label,
               '',
               '\\begin{tabular}{|c|c|c|c|}',
               '\\hline',
               '\\textbf{Combination} & \\textbf{LCP} &',
               '\\textbf{RCP} & \\textbf{Delta} \\\\',
               '\\hline \\hline']
        for test in tests:
            cps = test[1]
            delta = cps.delta()
            res.append(f'{test[0]} & {cps.lcp:4.1f} & '
                       f'{cps.rcp:4.1f} & {delta:5.2f}'+'\\\\')
            res.append('\\hline')
        res.append('\\end{tabular}\n')
        return '\n'.join(res)

    def hi_lo_report_both(self, label, hi_tests, lo_tests):
        """Present results of the Hi/Lo test (both Hi and Lo parts were done).
        """
        res = [label,
               '',
               '\\begin{tabular}{|c|c|c|c|c|}',
               '\\hline',
               '\\textbf{Combination} & \\textbf{LCP} &',
               '\\textbf{RCP} & \\textbf{Delta} & \\textbf{Avg Delta} \\\\',
               '\\hline']
        for hi_test, lo_test in zip(hi_tests, lo_tests):
            hi_cps, lo_cps = hi_test[1], lo_test[1]
            hi_delta, lo_delta = hi_cps.delta(), lo_cps.delta()
            delta = (lo_delta + hi_delta)/2.0
            res.append(f'{lo_test[0]} & {lo_cps.lcp:4.1f} & '
                       f'{lo_cps.rcp:4.1f} & {lo_delta:5.2f} & '+'\\\\')
            res.append('\\hline')
            res.append(f'{hi_test[0]} & {hi_cps.lcp:4.1f} & {hi_cps.rcp:4.1f}'
                       f' & {hi_delta:5.2f} & {delta:5.2f}'+'\\\\')
            res.append('\\hline')
        res.append('\\end{tabular}\n')
        return '\n'.join(res)

    def graph_report_action(self, graph):
        """Generate the latex code for the wheel action report.
        """
        action = graph.get_prop('wheel_action')
        if action is not None:
            res = ['\\textbf{Wheel action:} '+action]
        else:
            wheel_num = 1
            legend = list()
            actions = list()
            while True:
                action = graph.get_prop(f'wheel{wheel_num}_action')
                if action is None:
                    break
                legend.append('&\\textbf{Wheel '+f'{wheel_num}'+'}')
                actions.append(action)
                wheel_num += 1
            res = ['\\begin{adjustbox}{max width=\\textwidth}',
                   '\\begin{tabular}{'+f'{"c"*(len(actions)+1)}'+'}',
                   "".join(legend)+'\\\\',
                   '\\textbf{Action}&'+'&'.join(actions),
                   '\\end{tabular}',
                   '\\end{adjustbox}']
        return res

    def graph_save_pic(self, graph, viz):
        """Save the graph to an eps file and return its name.
        """
        number = graph.number
        graph_name = f'graph{int(number):02}.eps'
        viz.save_graph(graph.points, graph_name, 9.2, 6)
        return graph_name

    def graph_report(self, graph, viz):
        """Generate the graph report latex code.
        """
        graph_file = self.graph_save_pic(graph, viz)
        res = ['\\newpage',
               '\\begin{center}',
               '{\\huge Graph No.'+f'{int(graph.number)}'+'}',
               '',
               '\n'.join(self.graph_report_action(graph)),
               '\\rotatebox{90}{\\scalebox{.8}'
               '{\\includegraphics{'+graph_file+'}}}',
               '\\end{center}']
        if graph.hi_lo_test:
            res.append('\\newpage')
            for test in graph.hi_lo_test:
                res.append(hi_lo_test_report(test, self))
                res.append('\\vspace{5mm}')
            res.append('\\vspace{5mm}')
            res.append('')
        if graph.get_prop("conclusion"):
            res.append('\\textbf{Conclusion:} ' +
                       f'{graph.get_prop("conclusion")}')
        return '\n'.join(res)

    def manipulation_report(self, info, graph_reports):
        """Generate the latex code for the manipulation report.
        """
        res = ['\\documentclass[a4paper]{report}',
               '  \\usepackage{graphics, adjustbox}',
               '\\begin{document}',
               # '\\title{'+f'{info.get_prop("name")}'+' Manipulation}',
               # '%\\author{}',
               # '\\maketitle',
               '\\begin{titlepage}',
               '  \\begin{center}',
               '    \\vspace*{1cm}',
               '    %\\includegraphics[width=\\textwidth]{3330.png}',
               '    \\vspace{1cm}',
               '    \\huge',
               '    \\textbf{'+f'{info.get_prop("name")}'+' Manipulation}\\\\',
               '    \\vspace{1cm}',
               '    \\large',
               '    %\\textbf{author}',
               '    \\vfill',
               '    %2.8.2022',
               '  \\end{center}',
               '\\end{titlepage}\n',
               graph_reports,
               '\\end{document}']
        return '\n'.join(res)

    REPORT_EXTENSION = 'latex'


class HtmlBackend(BackEnd):
    """
    Html backend report generator class.
    """
    def __init__(self):
        #super().__init__()
        pass

    def hi_lo_report_single(self, label, tests):
        """Present results of either Hi or Lo test.
        """
        res = [label,
               '<table border="1" width="90%">',
               '<tr><td><b>Combination</b><td><b>LCP</b><td>',
               '<b>RCP</b><td><b>Delta</b></tr>']
        for test in tests:
            cps = test[1]
            delta = cps.delta()
            res.append(f'<tr><td>{test[0]} <td> {cps.lcp:4.1f} <td> '
                       f'{cps.rcp:4.1f} <td> {delta:5.2f}</tr>')
        res.append('</table>\n')
        return '\n'.join(res)

    def hi_lo_report_both(self, label, hi_tests, lo_tests):
        """Present results of the Hi/Lo test (both Hi and Lo parts were done).
        """
        res = [label,
               '<table border="1" width="90%">',
               '<tr><td><b>Combination</b><td><b>LCP</b><td>',
               '<b>RCP</b><td><b>Delta</b><td><b>Avg Delta</b></tr>']
        for hi_test, lo_test in zip(hi_tests, lo_tests):
            hi_cps, lo_cps = hi_test[1], lo_test[1]
            hi_delta, lo_delta = hi_cps.delta(), lo_cps.delta()
            delta = (lo_delta + hi_delta)/2.0
            res.append(f'<tr><td>{lo_test[0]} <td> {lo_cps.lcp:4.1f} <td> '
                       f'{lo_cps.rcp:4.1f} <td> {lo_delta:5.2f} </tr>')
            res.append(f'<tr><td>{hi_test[0]} <td> {hi_cps.lcp:4.1f} <td> {hi_cps.rcp:4.1f}'
                       f' <td> {hi_delta:5.2f} <td> {delta:5.2f}</tr>')
        res.append('</table>\n')
        return '\n'.join(res)

    def graph_report_action(self, graph):
        """Generate the latex code for the wheel action report.
        """
        action = graph.get_prop('wheel_action')
        if action is not None:
            res = [f'<b>Wheel action:</b> {action}',
                   '<p>']
        else:
            wheel_num = 1
            legend = list()
            actions = list()
            while True:
                action = graph.get_prop(f'wheel{wheel_num}_action')
                if action is None:
                    break
                legend.append('<b>Wheel '+f'{wheel_num}'+'</b>')
                actions.append(action)
                wheel_num += 1
            res = ['<table border="1" width="90%">',
                   '<tr><td></td><td>' + '</td><td>'.join(legend)+'</td></tr>',
                   '<tr><td><b>Action</b></td><td>' + '</td><td>'.join(actions)\
                   + '</td></tr>',
                   '</table>',
                   '<p>']
        return res

    def graph_save_pic(self, graph, viz):
        """Save the graph to an png file and return its name.
        """
        number = graph.number
        graph_name = f'graph{int(number):02}.png'
        viz.save_graph(graph.points, graph_name, 9.2, 6)
        return graph_name

    def graph_report(self, graph, viz):
        """Generate the graph report html code.
        """
        graph_file = self.graph_save_pic(graph, viz)
        res = ['<hr>',
               f'<h2> Graph No.{int(graph.number)}</h2>',
               '',
               '\n'.join(self.graph_report_action(graph)),
               f'<img src="{graph_file}" alt="Graph No.{int(graph.number)}"'
               f' width="100%">']
        if graph.hi_lo_test:
            res.append('<p>')
            for test in graph.hi_lo_test:
                res.append(hi_lo_test_report(test, self))
        if graph.get_prop("conclusion"):
            res.append('<p><b>Conclusion: ' +
                       f'{graph.get_prop("conclusion")}</b>')
        return '\n'.join(res)

    def manipulation_report(self, info, graph_reports):
        """Generate the latex code for the manipulation report.
        """
        res = ['<!DOCTYPE html>',
               '<html>',
               '<head>',
               '<title>'+f'{info.get_prop("name")}'+' Manipulation}</title>',
               '</head>',
               '<body>',
               '<title>'+f'{info.get_prop("name")}'+' Manipulation}</title>',
               '<p>',
               graph_reports,
               '</body>',
               '</html>']
        return '\n'.join(res)

    REPORT_EXTENSION = 'html'


def hi_lo_test_report(test, backend):
    """Present the Hi/Lo test results.
    The biggest problem was to identify which test is high and which low.
    The hi/low sort is determined by comparison with the default;
    then each group is sorted itself, although high test must be sorted
    in reverse.
    """
    hi_tests, lo_tests = test.get_test_results()

    if hi_tests and lo_tests:
        return backend.hi_lo_report_both('High/Low test', hi_tests, lo_tests)
    else:
        if lo_tests:
            return backend.hi_lo_report_single("Low test", lo_tests)
        return backend.hi_lo_report_single("High test", hi_tests)


class ManipulationReport:
    """Genrate the latex code for manipulation report.
    """
    def __init__(self, manipulation, backend):
        self.backend = backend
        self.manipulation = manipulation

    def gather_graphs(self):
        """Generate latex code for all the graphs.
        """
        graph_reports = list()
        limits = (self.manipulation.info.get_prop('min'),
                  self.manipulation.info.get_prop('max'))
        viz = manipulation_toolbox.GraphVisualization(limits)
        for dummy, graph in self.manipulation.graphs.items():
            graph_report = self.backend.graph_report(graph, viz)
            graph_reports.append(graph_report)
        return '\n\n'.join(graph_reports)

    def report(self):
        """Generate the latex code for the manipulation report.
        """
        info = self.manipulation.info
        graph_reports = self.gather_graphs()
        return self.backend.manipulation_report(info, graph_reports)

    def save_report(self, fname):
        """Save the report to a file.
        """
        with open(f'{fname}.{self.backend.REPORT_EXTENSION}', 'w') as file:
            file.write(self.report())

def main():
    """Entry point of the manipulation report generator.
    """

    arg_parser = argparse.ArgumentParser(description='Manipulation report '
                                                     'generator.')
    arg_parser.add_argument('-f', '--format', dest='format',
                            default='latex',
                            help='Format of the report (html/latex).')
    arg_parser.add_argument('-r', '--report', dest='report', default='report',
                            help='File name of the report to generate.')
    arg_parser.add_argument('-s', '--source', dest='source',
                            default='manipulation.txt',
                            help='File name of the report source file.')
    args = arg_parser.parse_args()

    backend = args.format

    if backend == 'html':
        backend = HtmlBackend()
    elif backend == 'latex':
        backend = LatexBackend()
    else:
        print(f'Available backends are: latex, html.\n')
        return

    source = args.source

    report = ManipulationReport(manipulation_parser.parse_manipulation(source), backend)
    report.save_report(args.report)


if __name__ == '__main__':
    main()
#
