#!/usr/bin/env python3
#
"""Module runs the manipulation.
It guards against the interrupt signal (Ctrl-C) and uncaught exceptions,
saving session log to allow quickly restore the previous state.
"""
import signal
import argparse
import os
from .manipulation_toolbox import ManipulationController, IO


def handler(signum, frame):
    """Handler for the interrupt signal (Ctrl-C).
    """
    print(f'Signal {signum} caught from frame {frame}.\n')
    IO.save_input('session_sig.txt')
    exit(1)

def main():
    """Entry point of the manipulation script.
       Gathers arguments passed from the commandline, path and
       runs the manipulation.
    """
    signal.signal(signal.SIGINT, handler)

    arg_parser = argparse.ArgumentParser(description='Manipulation helper.')
    arg_parser.add_argument('-s', '--session', dest='session')
    args = arg_parser.parse_args()

    path = os.path.dirname(__file__)

    try:
        if 'session' in args:
            IO.set_input_from_file(args.session)
        ctrl = ManipulationController(path)
        ctrl.prepare()
        ctrl.main()
        IO.save_input('session.txt')
    except Exception as ex:
        IO.save_input('session_ex.txt')
        raise ex


if __name__ == '__main__':
    main()
#
